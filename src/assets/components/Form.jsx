import React, { useState } from "react";
import "./Form.css";

function Form() {
  const [nameValue, setNameValue] = useState("");
  const [firstNameValue, setFirstNameValue] = useState("");
  const [mailValue, setMailValue] = useState("");
  const [ageValue, setAgeValue] = useState("");
  const [passwordValue, setPasswordValue] = useState("");
  const [ageError, setAgeError] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();

    function validate(input) {
      return input > 18;
    }
    if (validate(ageValue)) {
      setAgeError("");
    } else {
      setAgeError("vous devez être majeur");
    }
    /*
    if (ageValue < 18) {
      setAgeError("Vous devez être majeur.");
      return;
    } else {
      setAgeErrorMessage('');
    }
    */

    console.log(nameValue);
    console.log(firstNameValue);
    console.log(mailValue);
    console.log(ageValue);
    console.log(passwordValue);
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Prénom{" "}
        <input
          type="text"
          value={nameValue}
          onChange={(e) => setNameValue(e.target.value)}
        />
      </label>

      <label>
        Nom
        <input
          type="text"
          value={firstNameValue}
          onChange={(e) => setFirstNameValue(e.target.value)}
        />
      </label>

      <label>
        Email
        <input
          type="email"
          value={mailValue}
          onChange={(e) => setMailValue(e.target.value)}
        />
      </label>

      <label>
        Age
        <input
          type="number"
          value={ageValue}
          onChange={(e) => setAgeValue(e.target.value)}
        />
        {ageError && <p style={{ color: "red" }}>{ageError}</p>}
      </label>

      <label>
        Mot de passe
        <input
          type="password"
          value={passwordValue}
          onChange={(e) => setPasswordValue(e.target.value)}
        />
      </label>

      <button type="submit">Envoyer</button>
    </form>
  );
}

export default Form;
